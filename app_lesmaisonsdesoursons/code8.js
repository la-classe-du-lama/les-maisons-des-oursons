gdjs.Sc_232ne_32sans_32titreCode = {};
gdjs.Sc_232ne_32sans_32titreCode.localVariables = [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595validerObjects1= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595validerObjects2= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595retourObjects1= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595retourObjects2= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595chiffre_9595deObjects1= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595chiffre_9595deObjects2= [];
gdjs.Sc_232ne_32sans_32titreCode.GDtxt_9595representation_9595quantiteObjects1= [];
gdjs.Sc_232ne_32sans_32titreCode.GDtxt_9595representation_9595quantiteObjects2= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595rejouerObjects1= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595rejouerObjects2= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595scoreObjects1= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595scoreObjects2= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595faux_9595vraiObjects1= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595faux_9595vraiObjects2= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595aideObjects1= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595aideObjects2= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595maisonObjects1= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595maisonObjects2= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595fond_9595maison_95951Objects1= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595fond_9595maison_95951Objects2= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595fond_9595maison_95952Objects1= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595fond_9595maison_95952Objects2= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595quantiteObjects1= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595quantiteObjects2= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595etiquette_9595etObjects1= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595etiquette_9595etObjects2= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595ourson_95951Objects1= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595ourson_95951Objects2= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595decompositionObjects1= [];
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595decompositionObjects2= [];
gdjs.Sc_232ne_32sans_32titreCode.GDGreenFlatBarObjects1= [];
gdjs.Sc_232ne_32sans_32titreCode.GDGreenFlatBarObjects2= [];
gdjs.Sc_232ne_32sans_32titreCode.GDtsp_9595Objects1= [];
gdjs.Sc_232ne_32sans_32titreCode.GDtsp_9595Objects2= [];
gdjs.Sc_232ne_32sans_32titreCode.GDfondObjects1= [];
gdjs.Sc_232ne_32sans_32titreCode.GDfondObjects2= [];


gdjs.Sc_232ne_32sans_32titreCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
}

}


};

gdjs.Sc_232ne_32sans_32titreCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595validerObjects1.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595validerObjects2.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595retourObjects1.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595retourObjects2.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595chiffre_9595deObjects1.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595chiffre_9595deObjects2.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDtxt_9595representation_9595quantiteObjects1.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDtxt_9595representation_9595quantiteObjects2.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595rejouerObjects1.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595rejouerObjects2.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595scoreObjects1.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595scoreObjects2.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595faux_9595vraiObjects1.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595faux_9595vraiObjects2.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595aideObjects1.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595aideObjects2.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595maisonObjects1.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595maisonObjects2.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595fond_9595maison_95951Objects1.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595fond_9595maison_95951Objects2.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595fond_9595maison_95952Objects1.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595fond_9595maison_95952Objects2.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595quantiteObjects1.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595bouton_9595quantiteObjects2.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595etiquette_9595etObjects1.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595etiquette_9595etObjects2.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595ourson_95951Objects1.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595ourson_95951Objects2.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595decompositionObjects1.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDsp_9595decompositionObjects2.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDGreenFlatBarObjects1.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDGreenFlatBarObjects2.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDtsp_9595Objects1.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDtsp_9595Objects2.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDfondObjects1.length = 0;
gdjs.Sc_232ne_32sans_32titreCode.GDfondObjects2.length = 0;

gdjs.Sc_232ne_32sans_32titreCode.eventsList0(runtimeScene);

return;

}

gdjs['Sc_232ne_32sans_32titreCode'] = gdjs.Sc_232ne_32sans_32titreCode;
